import numpy as np
import cv2
from sklearn.datasets import fetch_olivetti_faces
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
import matplotlib.pyplot as plt

# The dataset comprises facial images of 40 distinct subjects, taken at different times and under different lighting conditions
# In addition, subjects varied their facial expression (open/closed eyes, smiling/not smiling) and their facial details (glasses/no glasses)
dataset = fetch_olivetti_faces()

X = dataset.data
y = dataset.target


np.random.seed(21)
idx_rand = np.random.randint(len(X), size=8)


# We can plot these example images using Matplotlib, but we need to make sure we reshape the column vectors to 64 x 64 pixel images before plotting:
plt.figure(figsize=(14, 8))
for p, i in enumerate(idx_rand):
    plt.subplot(2, 4, p + 1)
    plt.imshow(X[i, :].reshape((64, 64)), cmap='gray')
    plt.axis('off')

# We want to make sure that all example images have the same mean grayscale level:
n_samples, n_features = X.shape
X -= X.mean(axis=0)

# We repeat this procedure for every image to make sure the feature values of every data point (that is, a row in X) are centered around zero:
X -= X.mean(axis=1).reshape(n_samples, -1)

# The preprocessed data can be visualized using the preceding code:
plt.figure(figsize=(14, 8))
for p, i in enumerate(idx_rand):
    plt.subplot(2, 4, p + 1)
    plt.imshow(X[i, :].reshape((64, 64)), cmap='gray')
    plt.axis('off')
plt.savefig('olivetti-pre.png')


# Split the data into training and test sets:
X_train, X_test, y_train, y_test = train_test_split(
    X, y, random_state=21
)

# Prepare to apply a random forest 
rtree = cv2.ml.RTrees_create()

# create an ensemble with 50 decision trees
num_trees = 50
eps = 0.01
criteria = (cv2.TERM_CRITERIA_MAX_ITER + cv2.TERM_CRITERIA_EPS,
            num_trees, eps)
rtree.setTermCriteria(criteria)


# Because we have a large number of categories (that is, 40), we want to make sure the random forest is set up to handle them accordingly:
rtree.setMaxCategories(len(np.unique(y)))

rtree.setMinSampleCount(2)


rtree.setMaxDepth(1000)

#  fit the classifier to the training data:
rtree.train(X_train, cv2.ml.ROW_SAMPLE, y_train)

# check the resulting depth of the tree
rtree.getMaxDepth()

# The evaluation of the classifier is done once again by predicting the labels first (y_hat) and then passing them to the accuracy_score function:
_, y_hat = rtree.predict(X_test)

accuracy_score(y_test, y_hat)

# We find 87% accuracy, which turns out to be much better than with a single decision tree

tree = DecisionTreeClassifier(random_state=21, max_depth=25)
tree.fit(X_train, y_train)
tree.score(X_test, y_test)

# We can play with the optional parameters to see if we get better. The most important one seems to be the number of trees in the forest. We can repeat the experiment with a forest made from 100 trees:
num_trees = 100
eps = 0.01
criteria = (cv2.TERM_CRITERIA_MAX_ITER + cv2.TERM_CRITERIA_EPS, num_trees, eps)
rtree.setTermCriteria(criteria)
rtree.train(X_train, cv2.ml.ROW_SAMPLE, y_train)
_, y_hat = rtree.predict(X_test)
accuracy_score(y_test, y_hat)