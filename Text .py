from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
# When working with text features, it is often convenient to encode individual words or phrases as numerical values.
# Dataset
sample = [
    'feature engineering',
    'feature selection',
    'feature extraction'
]

# One of the simplest methods of encoding such data is by word count; for each phrase, we count the occurrences of each word within it
vec = CountVectorizer()
X = vec.fit_transform(sample)
print(X)

# By default, this will store our feature matrix X as a sparse matrix
print("\n")
print(X.toarray())

# with corresponding feature names:
print("\n")
print(vec.get_feature_names())

# One possible shortcoming of this approach is that we might put too much weight on words that appear very frequently
# One approach to fix this is known as term frequency-inverse document frequency (TF-IDF)
# # What TF-IDF does might be easier to understand than its name, which is basically to weigh the word counts by a measure of how often they appear in the entire dataset.
vec = TfidfVectorizer()
X = vec.fit_transform(sample)
print("\n")
print(X.toarray())

# We note that the numbers are now smaller than before, with the third column taking the biggest hit. 
# This makes sense, as the third column corresponds to the most frequent word across all three phrases
print("\n")
print(vec.get_feature_names())