from sklearn.datasets import load_digits
from sklearn.metrics import accuracy_score
import cv2
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
from scipy.stats import mode

# Loading the dataset
# The dataset consists of 1,797 samples with 64 features each
digits = load_digits()
digits.data.shape

# We tell the algorithm to perform at most 10 iterations and stop the process if our prediction of the cluster centers does not improve within a distance of 1.0:
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)
flags = cv2.KMEANS_RANDOM_CENTERS

# Since there are 10 different digits (0-9), we tell the algorithm to look for 10 distinct clusters:
compactness, clusters, centers = cv2.kmeans(digits.data.astype(np.float32), 10, None, criteria, 10, flags)

# Similar to the 𝑁×3
# matrix that represented different RGB colors, this time, the centers array consists of 𝑁×8×8 center images, where 𝑁 is the number of clusters. Therefore, if we want to plot the centers, we have to reshape the centers matrix back into 8 x 8 images:
plt.style.use('ggplot')
fig, ax = plt.subplots(2, 5, figsize=(10, 4))
centers = centers.reshape(10, 8, 8)
for axi, center in zip(ax.flat, centers):
    axi.set(xticks=[], yticks=[])
    axi.imshow(center, interpolation='nearest', cmap=plt.cm.binary)
plt.savefig('digits.png')

# 𝑘-means was able to partition the digit images not just into any 10 random clusters, but into the digits 0-9! In order to find out which images were grouped into which clusters, we need to generate a labels vector as we know it from supervised learning problems:
labels = np.zeros_like(clusters.ravel())
for i in range(10):
    mask = (clusters.ravel() == i)
    labels[mask] = mode(digits.target[mask])[0]

# calculate the performance of the algorithm using scikit-learn's accuracy_score metric:
accuracy_score(digits.target, labels)

# We can gain more insights about what went wrong and how by looking at the confusion matrix. 
# The confusion matrix is a 2D matrix 𝐶, where every element 𝐶𝑖,𝑗 is equal to the number of observations known to be in group (or cluster) 𝑖, but predicted to be in group 𝑗. Thus, all elements on the diagonal of the matrix represent data points that have been correctly classified (that is, known to be in group 𝑖 and predicted to be in group 𝑖). Off-diagonal elements show misclassifications.


confusion_matrix(digits.target, labels)


